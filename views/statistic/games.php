<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User`s matches details');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profile-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'game_name',
            [
                'attribute' => 'game_date_at',
                'value' => function($model) {
                    return date(Yii::$app->params['defaultDateFormat'], $model->game_date_at);
                }
            ],
            [
                'attribute' => 'Wins',
                'value' => function($model) use ($uid) {
                    if ($model->winner_user_id == $uid) {
                        return mb_strtoupper('win');
                    }
                    return '';
                }
            ],
            [
                'attribute' => 'Loses',
                'value' => function($model) use ($uid) {
                    if ($model->loser_user_id == $uid) {
                        return mb_strtoupper('lose');
                    }
                    return '';
                }
            ],

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
