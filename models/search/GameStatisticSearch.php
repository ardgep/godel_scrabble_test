<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\GameStatistic;

/**
 * GameStatisticSearch represents the model behind the search form about `app\models\GameStatistic`.
 */
class GameStatisticSearch extends GameStatistic
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['game_id', 'highScore', 'lowScore', 'winner_id', 'loser_id'], 'integer'],
            [['game_name'], 'string'],
            [['avgScore'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GameStatistic::find()
            ->joinWith('game as game');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $dataProvider->setSort([
            'attributes' => array_merge((new GameStatistic())->attributes(), [
                'game_name' => [
                    'asc' => [
                        'name' => SORT_ASC,
                    ],
                    'desc' => [
                        'name' => SORT_DESC,
                    ],
                ],
            ])
        ]);

        // grid filtering conditions
        $query->andFilterWhere([
            'highScore' => $this->highScore,
            'lowScore' => $this->lowScore,
            'avgScore' => $this->avgScore,
            'winner_id' => $this->winner_id,
            'loser_id' => $this->loser_id,
        ]);

        $query->andFilterWhere(['like', 'game.name', $this->game_name]);

        return $dataProvider;
    }
}
