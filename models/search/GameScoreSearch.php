<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\GameScore;

/**
 * GameScoreSearch represents the model behind the search form about `app\models\GameScore`.
 */
class GameScoreSearch extends GameScore
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'winner_user_id', 'loser_user_id', 'game_date_at'], 'integer'],
            [['game_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GameScore::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'winner_user_id' => $this->winner_user_id,
            'loser_user_id' => $this->loser_user_id,
            'game_date_at' => $this->game_date_at,
        ]);

        $query->andFilterWhere(['like', 'game_name', $this->game_name]);

        return $dataProvider;
    }
}
