<?php

namespace app\models;

use yii\base\Model;
use dektrium\user\models\LoginForm as BaseLoginForm;
use dektrium\user\helpers\Password;
use Yii;

class LoginForm extends BaseLoginForm
{

    public function beforeValidate()
    {
        if (Model::beforeValidate()) {
            $user = $this->finder->findUserByEmail(trim($this->login));
            $this->user = $user;
        }
        return true;
    }

    public function validatePassword($attribute, $params)
    {
        $this->addError($attribute, Yii::t('user', 'Invalid login or password2'));
    }

    public function rules()
    {
        $rules =  parent::rules();

        $rules['passwordValidate'] = [
            'password',
            function ($attribute) {
                if ($this->user === null || !Password::validate($this->password, $this->user->password_hash)) {
                    $this->addError($attribute, Yii::t('user', 'Invalid login or password'));
                }

                if ($this->user !== null && $this->user->role !== User::ROLE_ADMIN) {
                    $this->addError($attribute, Yii::t('user', 'Only Administrator can access this area.'));
                }
            }
        ];

        return $rules;
    }
}
