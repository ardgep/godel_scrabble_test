<?php

namespace app\models;

use app\models\query\GameQuery;
use app\models\traits\CountriesTrait;
use Yii;

/**
 * This is the model class for table "{{%game}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $game_date_at
 *
 * @property GameScore[] $gameScores
 */
class Game extends \yii\db\ActiveRecord
{
    use CountriesTrait;

    public $winner_name;
    public $loser_name;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%game}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['game_date_at', 'winner_name', 'loser_name'], 'safe'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'game_date_at' => Yii::t('app', 'Game Date At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGameScores()
    {
        return $this->hasMany(GameScore::className(), ['game_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGameStatistic()
    {
        return $this->hasOne(GameStatistic::className(), ['game_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return GameQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GameQuery(get_called_class());
    }
}
