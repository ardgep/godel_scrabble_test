<?php

namespace app\models;

use app\models\query\GameUserStatisticQuery;
use Yii;

/**
 * This is the model class for table "{{%game_user_statistic}}".
 *
 * @property integer $user_id
 * @property string $username
 * @property integer $wins
 * @property integer $loses
 * @property string $avgnum
 * @property string $avgScore
 */
class GameUserStatistic extends \yii\db\ActiveRecord
{
    public static function primaryKey()
    {
        return ['user_id'];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%game_user_statistic}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'wins', 'loses'], 'integer'],
            [['username'], 'required'],
            [['avgnum', 'avgScore'], 'number'],
            [['username'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'username' => Yii::t('app', 'Username'),
            'wins' => Yii::t('app', 'Wins'),
            'loses' => Yii::t('app', 'Loses'),
            'avgnum' => Yii::t('app', 'Avgnum'),
            'avgScore' => Yii::t('app', 'Avg Score'),
        ];
    }

    /**
     * @inheritdoc
     * @return GameUserStatisticQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GameUserStatisticQuery(get_called_class());
    }
}
