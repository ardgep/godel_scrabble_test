<?php

namespace app\models\query;

use app\models\GameUserStatistic;

/**
 * This is the ActiveQuery class for [[GameUserStatistic]].
 *
 * @see GameUserStatistic
 */
class GameUserStatisticQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return GameUserStatistic[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return GameUserStatistic|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
