<?php

namespace tests\codeception\unit\models;

use app\models\Game;
use yii\codeception\DbTestCase;

class GameTest extends DbTestCase
{
    public $appConfig = '@tests/_config.php';

    public function testSaveGame()
    {
        $game = new Game();

        //simulate form input
        $post = [
            'Game' => [
                'name' => 'test_game',
                'game_date_at' => date('Y-m-d H:i:s'),
            ],
        ];

        $this->assertTrue($game->load($post), 'Could not load POST data');
        $this->assertTrue($game->save(), 'Could not save model');
    }
}