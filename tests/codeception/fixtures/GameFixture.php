<?php

namespace test\codeception\fixtures;

use yii\test\ActiveFixture;

class GameFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Game';
}