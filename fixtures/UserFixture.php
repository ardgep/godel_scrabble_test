<?php

namespace app\fixtures;

use yii\test\ActiveFixture;
use app\models\User;

/**
 * UserFixture fixture
 */
class UserFixture extends ActiveFixture
{
    public $modelClass = 'app\models\User';
    public $dataFile = '@app/fixtures/data/User.php';

    protected function resetTable()
    {
        User::deleteAll();
    }
}